AmblrMonkeyRunnerTest
=====================

MonkeyRunner Tests for the Amblr project since the patch to 
Chimpchat by Minao is not been pickedup/approved yet thus
we have to use jython/python and monkeyRunner for more 
flexibility in testing what we want than ChimpChat and java.

Right now use MonkeyRunner bare, however may integrate the
aster framework(googlecode) to integrate with more features later in testing.

Architecture
============

We wire ViewServer integration, ideas borrowed from DT Milano. 

Cautions
========

There currently is a bug in EasyMonkeyDevice MonkeyDevice.getText:
http://dtmilano.blogspot.com/2012/01/monkeyrunner-testing-views-properties.html

EasyMonkeyDevice will look for text:mText when we actually have mText.
Patch has not been fully applied to a SDK release so will use a work around.

Credits
=======

DT Milano came up with the python code to integrate with ViewServer:
http://dtmilano.blogspot.com/2012/02/monkeyrunner-interacting-with-views.html

